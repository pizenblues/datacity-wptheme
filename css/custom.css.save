/* -------------
      general
----------------*/
body, html {
  width: 100%;
  height: 100%;
  font-family: 'Abel', sans-serif;
  font-weight: normal;
  color: #000
}

/* fuentes */
@font-face {
  font-family: KelsonBold;
  src: url(../fonts/kelsonSans/Kelson_Sans_Bold.otf) format("opentype");
}
@font-face {
  font-family: KelsonRegular;
  src: url(../fonts/kelsonSans/Kelson_Sans_Regular.otf) format("opentype");
}

.tag {
  background: #23AFE2;
  color: #fff;
  font-family: kelsonBold;
  border-style: none;
  border-radius: 0.3rem;
  font-size: 0.75em;
  letter-spacing: 1px;
  padding: 0.4rem 1.8rem;
  text-align: center;
}

.tag-sized {
  width: 15rem;
}

.text-black {
  color: #000
}
.text-white {
  color: #fff
}
.text-lightblue {
  color: #23afe1
}
.text-gray {
  color: #666
}

.kelsonBold {
  font-family: KelsonBold;
}
.kelsonRegular {
  font-family: KelsonRegular;
  font-weight: 600;
}
.kelsonLight {
  font-family: KelsonRegular;
  font-weight: 300;
}

/* fondos */
.bg-blue {
  background: #1787B7;
}
.bg-lightblue {
  background: #23AFE2;
}
.bg-black {
  background: #000;
}
.bg-white {
  background: #fff;
}
.bg-lead {
  background: #2C2B30;
}
.bg-gray {
  background: #333;
}
.bg-lightgray {
  background: #545454;
}

/* placeholders */
.ph-gray{
  background: #929493
}
.ph-lightgray{
  background: #ADADAD
}
.ph-darkgray{
  background: #848685
}
.ph-slide{
  background: #BCBABB
}
.ph-profile{
  background: #A6A4A5
}

hr {
  border-color: black;
}

a.nostyle {
  color: inherit;
}

a:hover.nostyle {
  text-decoration: none;
  opacity:0.9;
  transition:all 0.3s ease;
}

.main-wrapper {
  height: 70vh;
  position: relative;
}

.main {
  position: absolute;
  left: 0;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
}

.main h1{
  font-family: kelsonBold;
  font-size: 4em;
  text-align: center;
  letter-spacing: 0.6rem;
}

.main h3{
  text-align: center;
  margin-top: 0;
}

.box {
  width: 25rem;
  margin-left: auto;
  margin-right: auto;
}

/* -------------
    navegacion
----------------*/
.navbar-custom {
  z-index: 1;
  color: #fff;
  font-family: kelsonRegular;
  padding: 2.05em 4.3em 0em 4.3em;
  font-weight: bolder;
}

.navbar-regular {
  padding: 2.05em 4.3em;
  background: #2C2B30;
}

.navbar-toggler {
  font-weight: bolder;
  color: #fff
}

.navbar-custom a {
  color: #fff;
}

.nav-item a:hover {
  text-decoration: none;
  opacity:0.7;
  transition:all 0.3s ease;
}

.nav-item{
  line-height: 4.2;
}

.navbar-nav {
  margin-bottom: auto;
}

.navbar-nav > li {
  margin-right: 22px;
  letter-spacing: 1px;
}

.navbar-icon {
  margin-right: 5px;
}

#menu .navbar-icon {
  margin-right: 2px;
}

.btn-none {
  background: none;
  border-style: none;
  color: #fff;
  font-weight: bolder;
  outline: none !important;
  border: 0 !important;
  cursor: pointer;
}

.btn-none:hover {
  opacity:0.7;
  transition:all 0.3s ease;
}

.dropdown-toggle::after {
  display:none;
}

#mainMenu {
  top: 5.5rem;
  background: #2C2B30;
  border-style: none;
  border-radius: 0;
}

#mainMenu ul {
  padding: 0;
  text-align: center;
}

#mainMenu li {
  margin:0;
  padding:0;
  height: 3.5rem;
}

#mainMenu li:hover {
  background: none;
  opacity:0.7;
  transition:all 0.3s ease;
}

#mainMenu a {
  padding-top: 1rem;
  padding-bottom: 1rem;
  color: #fff;
  font-weight: bolder;
}

/* -------------
      hero
----------------*/
#cover {
  height: 100vh;
  position: relative; 
  background: rgba(0, 0, 0, 0.5);
}

.hero {
  height: 100vh;
  position: relative;
}

.carousel-item-big {
  position: relative;
}

.carousel-item-big img{
  height: 100vh;
}

.carousel-caption{
  /* bottom: unset;
  top: 45%; */
  left: 0;
  right: 0;
  top: 57%;
  transform: translateY(-50%);
}

.carousel-caption h5 {
  font-size: 4.5em;
  font-family: kelsonBold;
  letter-spacing: 1px;
}

.carousel-caption p {
  letter-spacing: 1px;
  font-weight: bolder;
  padding-left: 1rem;
  padding-right: 1rem;
}

.arrow {
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  width: 40px;
  /* bottom: 1.25rem; */
  bottom: 55px;
}

.arrow a:hover {
  opacity:0.7;
  transition:all 0.3s ease;
}

/* -------------
     secciones
----------------*/
section h1 {
  color: #fff;
  font-family: KelsonRegular;
  font-weight: 600;
  text-align: center;
  padding-top: 3rem;
  padding-bottom: 3rem;
  margin-bottom: 0;
  font-size: 3em;
  letter-spacing: 0.05rem;
}

section h3 {
  font-family: KelsonRegular;
  font-weight: 600;
  text-align: center;
  padding-top: 3.5rem;
  padding-bottom: 4rem;
  margin-bottom: 0;
  font-size: 1.6em;
  letter-spacing: 0.05rem;
}

section h3 a {
  color: #000;
}

section h3 a:hover {
  text-decoration: none;
  color: #23afe1;
  opacity:0.7;
  transition:all 0.3s ease;
}

section h3 .active {
  color: #23afe1
}

.fullvh {
  height: 100vh;
}

.almostfullvh {
  height: 70vh;
}

.section-content {
  height: calc(100% - 9.5rem);
}

/* -------------
    articulos
----------------*/
.paragraph {
  font-size: 1.3em;
  padding: 3rem 4rem;
}

.paragraph h4{
  font-size: 1em;
  color: #23AFE2;
  font-family: kelsonBold;
  margin-bottom: 1rem;
  margin-top: 1.5rem;
}

.paragraph h5{
  font-size: 1em;
  color: #000;
  font-family: kelsonBold;
  padding-top: 1rem;
}

.paragraph span {
  display: block;
  line-height: 1.8rem;
}

.paragraph p{
  margin: 0;
  line-height: 1.5rem;
}

.paragraph img{
  max-width: 100%;
  height: auto;
  margin-right: auto;
  margin-left: auto;
  display: block;
}

/* .paragraph img:has(.alignright){
  margin-right: 0;
  margin-left: auto;
} */


/* -------------
       about
----------------*/
#quote {
  padding-top: 2.5rem;
  padding-bottom: 2.5rem;
}

#quote p {
  font-size: 1.1em;
}

#formula {
  padding-top: 3.1rem;
  padding-bottom: 3.1rem;
  color: #000;
}

#formula h2 {
  font-size: 1.5em;
  letter-spacing: 0.07rem;
  margin-bottom: 2.3rem;
}

.formula-item {
  text-align: center;
  padding: 0.5rem;
}

.formula-item h3{
  font-size: 1.2em;
  padding-top: 1.2rem;
  line-height: 1.3rem;
}

.formula-item p{
  font-size: 1.2em;
  line-height: 1.2rem;
  margin-bottom: 0.5rem;
}

.formula-flecha {
  position: relative;
}

.formula-container > div > div > div {
  position: absolute;
  width: 100%;
  bottom: 0.8rem;
}

.formula-container > div:not(:first-child) > div > div:before {
  content: " ";
  position: absolute;
  left: -13px;
  width: 50%;
  height: 1px;
  background-color: #a7a8a9;
}

.formula-container > div:not(:last-child) > div > div:after {
  content: " ";
  position: absolute;
  right: -13px;
  width: 50%;
  height: 1px;
  background-color: #a7a8a9;
}

/* -------------
     thumbs
----------------*/
.thumb {
  padding-left: 1rem;
  padding-right: 1rem;
  display: flex;
  align-items: center;
}

/* .thumb:hover {
  opacity:0.85;
  transition:all 0.3s ease;
} */

.thumb a {
  background: #f3f3f3;
  display: block;
  color: #000;
  width: 100%;
  text-align: center;
  border-radius: 0.2rem;
  padding: 0.5rem 0.5rem;
  margin: 0.5rem;
  font-size: 1.2em;
}

.thumb a:hover {
  text-decoration: none;
  background: #fff;
  color: #333;
}

/* -------------
    proyectos
----------------*/
#proyectos a {
  font-size: 1em;
}

/* -------------
    clientes
----------------*/
#clientes {
  /* height: auto; */
  /* height: 40rem; */
}

.client-wrapper {
  height: 40rem;
}

#clientes .carousel-item, .carousel, .carousel-inner {
  /* height: 35rem; */
  width: 100%;
}
/* foto del cliente con el circulito */
.cliente-profile p:nth-child(2) img{
  border: solid #23afe1 5px;
  border-radius: 50%;
  height: 65px;
  width: 65px;
}
/* logo fijo */
.cliente-profile p:first-child img{
  height: 119px;
  width: auto;
}

.cliente-profile p {
  font-size: 1.2em;
}

.cliente-profile b {
  display: block;
  text-transform: uppercase;
}

.client-name {
  font-size: 1.1em;
}

/* -------------
    contacto
----------------*/
.header {
  color: white;
  font-family: KelsonRegular;
  font-weight: 600;
  text-align: center;
  padding-top: 3rem;
  padding-bottom: 3rem;
  margin-bottom: 0;
}

.header h1 {
  padding: 0;
}

.header span {
  display: block;
}

.map-world {
  background-image: url("../img/worldmap.png");
  background-position: 0px 0px;
  height: 301px;
  display: block;
}

#map {
  height: 425px;
  width: 100%;
}

.map-globe {
  font-size: 1.3em;
}

.map-globe br{
  display: none;
}

.map-social a {
    font-size: 1.2em;
    margin-right: 0.5rem;
}

.map-social a:hover {
  text-decoration: none;
  opacity:0.7;
  transition:all 0.3s ease;
}

.map-social a.twitter {
  color: #53BBD8;
  font-size: 1.4em;
}

.map-social a.facebook {
  color: #226DBE;
  font-size: 1.4em;
}

.whatsapp {
  color: #2aa22d
}

.twitter {
  color: #53BBD8;
}

.facebook {
  color: #226DBE;
}

/* -------------
      footer
----------------*/
#footer .container {
  padding: 3rem 4rem 0em 4em;
}

footer {
  letter-spacing: 1px;
}

footer b {
  font-family: kelsonBold;
  font-size: 1em;
}

footer p {
  margin: 0;
  display: inline-block;
}

footer span {
  color: rgb(131, 131, 131);
  letter-spacing: 0px;
  font-size: 0.9em;
}

.siguenos > div {
  margin-right: 0.5rem;
}

.siguenos a:hover {
  text-decoration: none;
  opacity:0.7;
  transition:all 0.3s ease;
}

.totop {
  width: 100%;
  padding-right: 4rem;
  /* height: 3em; */
}

.totop-button{
  background: #23AFE2;
  width: 2.8rem;
  height: 2.5rem;
  color: white;
  padding-top: 0.5rem;
  text-align: center;
  border-radius: 5px;
  margin-top: 1rem;
  border-bottom-left-radius: 0px;
  border-bottom-right-radius: 0px;
}

.totop-button:hover{
  background: #1787B7;
  transition:all 0.3s ease;
}

/* -------------
  media queries
----------------*/
@media (max-width: 1052px){
  .navbar-custom {
    padding: 1em 1em 1em 1em;
  }

  .map-world {
    background-position:1270px 0px;
  }
}

/* menu collapse */
@media (max-width: 991px){
  #cover nav{
    background: linear-gradient(rgba(0,0,0,1), rgba(0,0,0,0)) !important;
  }

  .navbar-regular {
    padding-bottom: 0.7rem;
  }

  .navbar-nav > li {
    margin-right: auto;
    margin-left: auto;
    height: 3rem;
  }

  .collapse {
    padding-bottom: 2rem;
  }

  #menu, #mainMenu {
    display: none
  }

  .paragraph {
    padding: 3rem 1rem
  }
}
/* dispositivos moviles */
@media (max-width: 600px) {
  .navbar-nav {
    line-height: inherit;
  }

  .navbar-logo{
    margin-left: auto;
    margin-right: auto;
  }

  .navbar-toggler{
    margin-left: auto;
    margin-right: auto;
  }

  .carousel-caption h5{
    font-size: 2.8em;
  }

  .tag {
    font-size: 0.6em;
    padding: 0.4rem;
    width: 70%;
    display: none
  }

  #proyectos .section-content {
    height: calc(100% - 9.5rem);
  }

  .fullvh {
    height: 47rem;
  }

  /* #cover, .hero, .carousel-item-big img {
    height: 33rem;
  } */

  .almostfullvh {
    height: 40rem;
  }

  .map-world {
    background-position: 1150px 0px;
  }

  .map-globe br{
    display: block;
  }

  #footer .container {
    padding: 3rem 3rem
  }

  .box {
    width: 15rem;
  }
}

@media (max-width: 476px) {
  #proyectos .section-content {
    height: calc(100% - 13rem);
  }
}

/* responsive altura */
@media (max-height: 600px) {
  .carousel-caption h5{
    font-size: 2em;
    padding-top: 20px;
  }

  .navbar-nav > li {
    height: 2rem;
  }

  .fullvh {
    height: auto;
  }

  #cover, .hero, .carousel-item-big img {
    height: auto;
  }

  .almostfullvh {
    height: 40rem;
  }

  .main-wrapper {
    height: 20rem;
  }
}

@media (max-height: 350px) {
  .hero {
    display: block;
  }

  .carousel-caption h5{
    padding-top: 0;
  }

  .carousel-caption p{
    font-weight: bold;
    font-size: 0.8em;
  }
}

/* extra small stuff */
@media (max-width: 255px) {
  .section-content {
    height: calc(100% - 8.4rem);
  }

  #participamos .section-content {
    height: calc(100% - 8.3rem);
  }

  #proyectos .section-content {
    height: calc(100% - 10rem);
  }

  .map-world {
    background-position: 1015px 0px;
  }

  .subtitle-nav .subtitle-link {
    top: 60%;
  }

  section h1 {
    font-size: 2em;
  }

  section h3 {
    font-size: 1em;
  }

  section h4 {
    font-size: 0.8em;
  }

  section p, section span, .cliente-profile b {
    font-size: 0.8em;
  }

  .cliente-wrapper {
    padding: 0
  }
}
