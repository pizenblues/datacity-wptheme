<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */
get_header();
// $container   = get_theme_mod( 'understrap_container_type' );
// $sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<section id="top">
    <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<div class="main-wrapper position-relative">
  <div class="main">
    <h1 class="text-gray">404</h1>
    <h3>P&aacute;gina no encontrada</h3>
  </div>
</div>

<?php get_footer(); ?>
