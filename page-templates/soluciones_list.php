<?php
/**
 * Template Name: soluciones_lista
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<section id="top">
    <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<?php
$soluciones_query = new WP_Query( array( 
  'category_name' => 'soluciones',  
  'orderby' => 'publish_date',
  'order' => 'DESC',
));
?>
<section id="soluciones">
  <h1 class="bg-gray">Soluciones</h1>
  <div id="participamos-list" class="container-fluid">
    <div class="row h-100">
      <?php if ( $soluciones_query->have_posts() ) : ?>
      <?php $n = 0;
        while ( $soluciones_query->have_posts() ) : $soluciones_query->the_post(); ?>
          <div class="col-md-4 thumb <?php echo $n %2 == 0 ? 'ph-lightgray' : 'ph-gray'; ?>"
          style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full')[0]; ?>)">
            <a href="<?php echo the_permalink(); ?>">
              <?php the_title(); ?>
            </a>
          </div>
        <?php $n++; endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>