<?php
  get_header();
  $container = get_theme_mod( 'understrap_container_type' );
?>

<section id="top" class="d-print-none">
  <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<section>
  <h1 class="bg-blue">
    <a class="nostyle" href="<?php echo get_page_link(get_page_by_path('soluciones'));?>">Soluciones</a>
  </h1>
  <!-- titulo y subtitulo -->
  <h3 class="bg-lightgray text-white subtitle-nav">
    <div class="subtitle-main">
      - 
      <?php while ( have_posts() ) : the_post(); ?>
        <?php the_title(); ?>
      <?php endwhile;?>
      -
      <div class="subtitle-caption text-lightblue text-uppercase">
        <?php if(get_field('subtitulo')): ?>
          <?php the_field('subtitulo'); ?>
        <?php endif; ?>
      </div>
    </div>
    <!-- navegacion entre los posts -->
    <span class="xx d-print-none container-fluid">
      <div class="row">
        <div class="col text-left">
          <?php previous_post_link( '%link', '< SOLUCI&Oacute;N ANTERIOR', TRUE ); ?>
        </div>
        <div class="col text-right">
          <?php next_post_link( '%link', 'SIGUIENTE SOLUCI&Oacute;N >', TRUE ); ?>
        </div>
      </div>
    </span>
  </h3>
  <!-- slide -->
  <div class="d-print-none">
    <?php $slide = get_field('slide'); ?>
    <?php if($slide): ?>
      <?php echo do_shortcode($slide);?>
    <?php endif; ?>
  </div>
  <!-- campos de la descripcion -->
  <div class="container paragraph pb-2">
    <h4 class="px-4">DESCRIPCI&Oacute;N DE LA SOLUCI&Oacute;N</h4>
    <div class="row px-4">
      <div class="col-lg mb-3 mb-lg-0">
        <div class="article-desc">
          <span>
            <?php while ( have_posts() ) : the_post(); ?>
              <?php the_title(); ?>
            <?php endwhile;?>
          </span>
          <span class="text-uppercase">
            CLIENTE:
            <?php if (get_field('cliente')): ?>
              <?php the_field('cliente') ?>
            <?php endif; ?>
          </span>
          <span class="text-uppercase">
            SOLUCI&Oacute;N:
            <?php if (get_field('solucion')): ?>
              <?php the_field('solucion') ?>
            <?php endif; ?>
          </span>
        </div>
      </div>
      <!-- flechita y links sociales -->
      <div class="col-lg-3 d-print-none">
        <!-- condicionar flecha -->
        <div class="pointer-wrapper">
          <div class="pointer-tip pointer-warning"></div>
          <div class="pointer bg-tag-warning">
            NUEVA SOLUCI&Oacute;N
          </div>
        </div>
        <div class="article-share d-flex justify-content-center justify-content-lg-end mt-4">
          <a href="mailto:?Subject=<?php the_title(); ?>&Body=<?php the_permalink(); ?>">
            <i class="fa fa-envelope-o text-gray"></i>
          </a>
          <a title="Imprimir articulo" 
            href="javascript:void(0)" 
            onclick="window.print()">
            <i class="fa fa-print text-gray"></i>
          </a>
          <a target="_blank" 
            href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>">
            <i class="fa fa-twitter twitter"></i>
          </a>
          <a target="_blank" 
            href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>">
            <i class="fa fa-facebook-square facebook"></i>
          </a>
        </div>
      </div>
    </div>
    <hr class="my-4">
    <!-- contenido del post -->
    <div id="content">
      <p class="article px-4 pb-5">
        <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile;?>
      </p>
    </div>
  </div>
  <div class="container paragraph d-print-none">
    <div class="text-center mt-4 mb-5">
      <span class="mb-2">
        SI EST&Aacute;S INTERESADO, ESCR&Iacute;BENOS A:
      </span>
      <div class="tag tag-sized mx-auto">
        <a class="nostyle" target="_blank" 
          href="mailto:INFO@DATACITYIOT.COM">
          INFO@DATACITYIOT.COM
        </a>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
