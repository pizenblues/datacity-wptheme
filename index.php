<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
?>

<div id="top"></div>
<section id="cover">
  <?php get_template_part( 'global-templates/topnav' ); ?>
  <?php
    $principales_query = new WP_Query( array( 
      'category_name' => 'principales',  
      'orderby'  => array( 'meta_value_num' => 'ASC' ),
      'meta_key' => 'orden',
    ));
  ?>
  <div id="main-carousel" 
    class="carousel carousel-fade"
    data-interval="4000"
    data-pause="false"
    data-ride="carousel">
    <div class="carousel-inner">
    <?php if ( $principales_query->have_posts() ) : ?>
      <?php $n = 0;
      while ( $principales_query->have_posts() ) : $principales_query->the_post(); ?>
        <div
        class="carousel-item carousel-item-big <?php echo $n == 0 ? 'active' : '' ?> "
	      style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full')[0]; ?>)">
          <?php// the_post_thumbnail('full'); ?>
          <span class="d-block"></span>
          <div class="carousel-caption d-block">
            <h5><?php the_title(); ?></h5>
            <p><?php the_content(); ?></p>
          </div>
        </div>
        <?php $n++; endwhile; ?>
      <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
    <a class="carousel-control-prev" href="#main-carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#main-carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    <div class="arrow">
      <a href="#about">
        <img src="<?php echo get_template_directory_uri(); ?>/img/icono-flecha.png" alt="Ver mas">
      </a>
    </div>
  </div>
</section>
<section id="about">
  <h1 class="bg-gray">Datacity IoT</h1>
  <div id="quote" class="container-fluid bg-lightgray text-white">
    <div class="row">
      <div class="col-sm-2 text-center text-sm-right mb-3 mb-sm-0">
        <img src="<?php echo get_template_directory_uri(); ?>/img/quote-open.png">
      </div>
      <div class="col-sm p-0">
        <p class="kelsonRegular px-3 px-xl-0 d-xl-flex flex-wrap text-center">
          <span class="mx-sm-auto">
            SOMOS UNA EMPRESA CHILENA, ESPECIALISTAS EN DISEÑAR,
            DESARROLLAR  Y COMERCIALIZAR VARIADAS
          </span> 
          <span class="mx-sm-auto">
            "SOLUCIONES TECNOLOGICAS IoT", LLEVANDO A CABO SOLUCIONES
            "CLOUD" ROBUSTAS Y FIABLES ORIENTADAS
          </span>
          <span class="mx-sm-auto">
            AL MUNDO "SMART CITY" E "INDUSTRIA 4.0",
            ENTRE OTROS.
          </span>
        </p>
      </div>
      <div class="col-sm-2 text-center text-sm-left">
        <img src="<?php echo get_template_directory_uri(); ?>/img/quote-close.png">
      </div>
    </div>
  </div>
  <div id="formula">
    <h2 class="kelsonBold text-lightblue text-center">
      - NUESTRA F&Oacute;RMULA -
    </h2>
    <div class="container">
      <div class="row formula-container">
        <div class="col-sm formula-item">
          <img src="<?php echo get_template_directory_uri(); ?>/img/formula1.png">
          <h3 class="kelsonBold pb-1 pb-1">
            REALIZAR <br>
            ESTUDIO
          </h3>
          <p>
            Desarrollo de Estudio <br>
            previo, buscando los <br>
            mejores resultados.
          </p>
          <div class="formula-flecha">
            <img class="d-none d-sm-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrow.png">
            <img class="d-sm-none" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrowdown.png">
            <div class="line d-none d-lg-block"></div>
          </div>
        </div>
        <div class="col-sm formula-item mb-3 mb-sm-0">
          <img src="<?php echo get_template_directory_uri(); ?>/img/formula2.png">
          <h3 class="kelsonBold pb-1">
            DESARROLLAR <br>
            TECNOLOG&Iacute;A
          </h3>
          <p>
            Dise&ntilde;o e Ingenier&iacute;a <br>
            a medida de todas <br>
            tus necesidades.
          </p>
          <div class="formula-flecha">
            <img class="d-none d-sm-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrow.png">
            <img class="d-sm-none" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrowdown.png">
            <div class="line d-none d-lg-block"></div>
          </div>
        </div>
        <div class="col-sm formula-item">
          <img src="<?php echo get_template_directory_uri(); ?>/img/formula3.png">
          <h3 class="kelsonBold pb-1">
            INTEGRAR <br>
            SOLUCIONES
          </h3>
          <p>
            Integrar m&uacute;ltiples y <br>
            variados sensores y <br>
            perif&eacute;ricos.
          </p>
          <div class="formula-flecha">
            <img class="d-none d-sm-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrow.png">
            <img class="d-sm-none" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrowdown.png">
            <div class="line d-none d-lg-block"></div>
          </div>
        </div>
        <div class="col-sm formula-item">
          <img src="<?php echo get_template_directory_uri(); ?>/img/formula4.png">
          <h3 class="kelsonBold pb-1">
            PROTOCOLOS DE <br>
            COMUNICACI&Oacute;N
          </h3>
          <p>
            M&uacute;ltiples protocolos <br>
            de comunicaci&oacute;n <br>
            robustos y confiables.
          </p>
          <div class="formula-flecha">
            <img class="d-none d-sm-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrow.png">
            <img class="d-sm-none" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrowdown.png">
            <div class="line d-none d-lg-block"></div>
          </div>
        </div>
        <div class="col-sm formula-item">
          <img src="<?php echo get_template_directory_uri(); ?>/img/formula5.png">
          <h3 class="kelsonBold pb-1">
            FIRMWARE <br>
            A MEDIDA
          </h3>
          <p>
            Sistema IoT conectado <br>
            a nuestra plataforma <br>
            Cloud Services.
          </p>
          <div class="formula-flecha">
            <img class="d-none d-sm-block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrow.png">
            <img class="d-sm-none" src="<?php echo get_template_directory_uri(); ?>/img/formulaarrowdown.png">
            <div class="line d-none d-lg-block"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
$soluciones_query = new WP_Query( array( 
  'category_name' => 'soluciones',  
  'orderby'  => array( 'meta_value_num' => 'ASC' ),
  'posts_per_page' => 3,
  'meta_key' => 'orden'
));
?>
<section id="soluciones" class="fullvh">
  <h1 class="bg-lightblue">
    <a class="nostyle" href="<?php echo get_page_link(get_page_by_path('soluciones')); ?>">Soluciones</a>
  </h1>
  <div class="section-content container-fluid">
  <div class="row h-100">
  <?php if ( $soluciones_query->have_posts() ) : ?>
    <?php 
    $n = 0;
    while ( $soluciones_query->have_posts() ) : $soluciones_query->the_post(); ?>
      <div class="col-md thumb <?php echo $n %2 == 0 ? 'ph-lightgray' : 'ph-gray'; ?> "
        style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full')[0]; ?>)">
        <a href="<?php echo the_permalink(); ?>">
          <?php the_title(); ?>
        </a>
      </div>
    <?php $n++; endwhile; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>
  </div>
  </div>
</section>

<?php
$proyectos_query = new WP_Query( array( 
  'category_name' => 'proyectos',  
  'orderby'  => array( 'meta_value_num' => 'ASC' ),
  'posts_per_page' => 4,
  'meta_key' => 'orden'
));
?>
<section id="proyectos" class="almostfullvh">
  <h1 class="bg-black">
    <a class="nostyle" href="<?php echo get_page_link(get_page_by_path('proyectos')); ?>">Proyectos Destacados</a>
  </h1>
  <div class="section-content container-fluid">
  <div class="row h-100">
  <?php if ( $proyectos_query->have_posts() ) : ?>
    <?php 
    $n = 0;
    while ( $proyectos_query->have_posts() ) : $proyectos_query->the_post(); ?>
      <div class="col-md thumb <?php echo $n %2 == 0 ? 'ph-gray' : 'ph-darkgray'; ?> "
      style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full')[0]; ?>)">
        <a href="<?php echo the_permalink(); ?>">
          <?php the_title(); ?>
        </a>
      </div>
    <?php $n++; endwhile; ?>
    <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  </div>
  </div>
</section>

<?php
$participamos_query = new WP_Query( array( 
  'category_name' => 'participamos',  
  'orderby'  => array( 'meta_value_num' => 'ASC' ),
  'posts_per_page' => 3,
  'meta_key' => 'orden'
));
?>
<section id="participamos" class="fullvh">
  <h1 class="bg-lightblue">
    <a class="nostyle" href="<?php echo get_page_link(get_page_by_path('participamos')); ?>">Participamos</a>
  </h1>
  <div class="section-content container-fluid">
  <div class="row h-100">
  <?php if ( $participamos_query->have_posts() ) : ?>
    <?php 
    $n = 0;
    while ( $participamos_query->have_posts() ) : $participamos_query->the_post(); ?>
      <div class="col-md thumb <?php echo $n %2 == 0 ? 'ph-lightgray' : 'ph-gray'; ?> "
      style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full')[0]; ?>)">
        <a href="<?php echo the_permalink(); ?>">
          <?php the_title(); ?>
        </a>
      </div>
    <?php $n++; endwhile; ?>
    <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  </div>
  </div>
</section>

<?php
  $testimonios_query = new WP_Query( array( 
    'category_name' => 'testimonios',  
    'orderby'  => array( 'meta_value_num' => 'ASC' ),
    'meta_key' => 'orden',
  ));
?>
<section id="clientes">
  <h1 class="bg-lightgray">Clientes</h1>
  <div class="client-wrapper d-flex align-items-center">
    <div id="cliente-carousel" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
        <?php if ( $testimonios_query->have_posts() ) : ?>
        <?php $i = 0;
        while ( $testimonios_query->have_posts() ) : $testimonios_query->the_post(); ?>
          <div class="carousel-item text-center <?php echo $i == 0 ? 'active' : '' ?> ">
            <!-- imagenes -->
            <div class="cliente-profile">
              <!-- logo del cliente -->
              <?php $logo = get_field('logo_empresa'); ?>
              <?php if ($logo): ?>
                <img src="<?php echo $logo['url']; ?>"
                  class="cliente-logo"
                  alt="Logo del cliente" />
              <?php endif; ?>
              <!-- imagen del cliente -->
              <?php $perfil = get_field('cliente_imagen'); ?>
              <?php if ($perfil): ?>
                <img src="<?php echo $perfil['url']; ?>"
                  class="cliente-imagen"
                  alt="Imagen del cliente" />
              <?php else: ?>
                <img src="http://via.placeholder.com/85" class="cliente-imagen">
              <?php endif; ?>
            </div>
            <!-- testimonio y quotes -->
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-3 my-2 my-sm-0 text-center text-sm-right">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quote-open.png">
                </div>
                <div class="col-sm">
                <?php if(get_field('cliente_testimonio')): ?>
                  <?php the_field('cliente_testimonio'); ?>
                <?php endif; ?>
                </div>
                <div class="col-sm-3 my-2 my-sm-0 text-center text-sm-left">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quote-close.png">
                </div>
              </div>
            </div>
            <div class="cliente-profile kelsonBold">
              <!-- nombre -->
              <b class="client-name text-lightblue">
                <?php if(get_field('cliente')): ?>
                  <?php the_field('cliente'); ?>
                <?php endif; ?>
              </b>
              <!-- cargo -->
              <b>
                <?php if(get_field('cliente_cargo')): ?>
                  <?php the_field('cliente_cargo'); ?>
                <?php endif; ?>
              </b>
            </div>
          </div>
          <?php $i++; endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </div>
      <!-- controles -->
      <a class="carousel-control-prev" href="#cliente-carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#cliente-carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</section>

<section id="contacto">
  <div class="header bg-black">
    <h1>Contacto</h1>
    <span>
      - SOLICITA NUESTRAS SOLUCIONES -
    </span>
  </div>
  <div class="map-world"></div>
  <div id="map" class="bg-lightgray"></div>
  <div class="container paragraph">
    <h4 class="text-lightblue kelsonBold px-2">
      - CHILE -
    </h4>
    <hr>
    <div class="d-flex pl-4">
      <div class="mr-2">
        <img class="mt-2" src="<?php echo get_template_directory_uri(); ?>/img/marker.png">
      </div>
      <div class="w-100">
        <b class="kelsonBold align-top">
          SANTIAGO
        </b>
        <p class="m-0">
          AV. ECHEÑIQUE 5839, OF 512, LA REINA,
          SANTIAGO, REGI&Oacute;N METROPOLITANA
        </p>
        <div class="d-md-flex justify-content-between">
          <div id="telefono" class="kelsonBold d-flex">
            <div class="pt-2 pt-md-0 pr-1">
              <img src="<?php echo get_template_directory_uri(); ?>/img/icono-phone-alt.png" 
                class="navbar-icon" 
                alt="Telefono">
            </div>
            <div>
              <div class="mr-2 mr-md-0 d-block d-md-inline">
                +562 29935452
              </div>
              <div class=" d-block d-md-inline">
                +562 29935453
              </div>
            </div>
          </div>
          <div class="tag mt-2 mt-md-0">
            <a class="nostyle" href="mailto:INFO@DATACITYIOT.COM">
              INFO@DATACITYIOT.COM
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
