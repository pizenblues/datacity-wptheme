// inicializando google maps en la direccion deseada
function initMap() {
  var lat = -33.445519;
  var lng = -70.570694;
  var address = {lat: lat, lng: lng};
  var center = {lat: -33.444264, lng: -70.570842};
  var map = new google.maps.Map(document.getElementById('map'), {
    center: center,
    zoom: 16,
  },);

  // posicion del marcadorcito
  var marker = new google.maps.Marker({
    position: address,
    icon: templateUrl+'/img/marker-map.png',
    map: map
  });
  
  // despues de 5 segundos vuelve al centro del marcador
  // cuando cambia la posicion del mapa
  map.addListener('center_changed', function() {
    window.setTimeout(function() {
      map.panTo(center);
    }, 5000);
  });

  // globo de texto con la direccion
  var mapSticky = 
  '<div class="map-globe p-3">'+
    '<b class="kelsonBold">Datacity IoT</b>'+
    '<p>'+
      'AV. ECHEÑIQUE 5839, OF 512, LA REINA, SANTIAGO - CHILE'+
    '</p>'+
    '<div class="map-social">'+
      '<a class="twitter" target="_blank" '+
      'href="http://twitter.com/home/?status=https%3A%2F%2Fgoo.gl%2Fmaps%2FHLErokSh5GC2">'+
      '<i class="fa fa-twitter twitter"></i>'+
      '</a>'+
      '<a class="facebook" target="_blank" '+
      'href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fgoo.gl%2Fmaps%2FHLErokSh5GC2">'+
        '<i class="fa fa-facebook-square facebook"></i>'+
      '</a>'+
      '<a target="_blank" href="https://waze.com/ul?ll='+lat+','+lng+'&navigate=yes">'+
        '<img src="'+templateUrl+'/img/icono-car.png" class="pb-2">'+
      '</a>'+
    '</div>'+
  '</div>';

  // crea un objeto infoWindow con el contenido 
  // del globo de texto en mapSticky
  var infowindow = new google.maps.InfoWindow({
    content: mapSticky
  });

  // agrega el globo al marcador
  function attachMessage(marker, message) {
    marker.addListener('click', function() {
      infowindow.open(marker.get('map'), marker);
    });
  }
  
  // llama a la funcion para mostrar el globo 
  // al hacer click en el marcador
  attachMessage(marker, mapSticky);

  // carga el mensaje por default
  google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
    infowindow.open(map, marker);
  });
}